# minimal-mistakes

Minimal Mistakes Jekyll theme demo (v4.24.0) for GitLab Pages: <https://nithiya.gitlab.io/minimal-mistakes>

Repository: <https://gitlab.com/nithiya/minimal-mistakes>

## Serving this site locally

1.  Install Ruby.

2.  Clone this repository:

    Option 1 - SSH:

    ```sh
    git clone git@gitlab.com:nithiya/minimal-mistakes.git
    ```

    Option 2 - HTTPS:

    ```sh
    git clone https://gitlab.com/nithiya/minimal-mistakes.git
    ```

3.  Navigate to the cloned directory and install all requirements:

    ```sh
    cd minimal-mistakes
    gem install bundler
    bundle install
    ```

4.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

5.  View the site at `http://localhost:4000/minimal-mistakes/`.

## License

[MIT License](https://opensource.org/licenses/MIT)

This repository is maintained by Nithiya Streethran (nmstreethran at gmail dot com)

## Credits

- [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/)
- [Jekyll](https://jekyllrb.com/)
- [Ruby programming language](https://www.ruby-lang.org/en/)
- [Lunr.js](https://lunrjs.com/)
- [Staticman](https://staticman.net/)
- [Solarized](https://ethanschoonover.com/solarized/)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Project Gutenberg](https://www.gutenberg.org/)
- [reCAPTCHA](https://www.google.com/recaptcha/about/)
- [Fly](https://fly.io/)
- [Akismet](https://akismet.com)
