---
title: Excerpt (Defined)
excerpt: >
  This is a user-defined post excerpt. It should be displayed in place of the
  post content in archive-index pages.
header:
  show_overlay_excerpt: true
  overlay_image: assets/images/ingredients.jpg
  overlay_filter: .3
  image_description: Food for medieval meal by Colleen Galvin
  caption: "*[Colleen Galvin. Food for medieval meal. Flickr. CC-BY-2.0.](https://flic.kr/p/c25o7N)*"
categories:
  - layout
  - uncategorised
tags:
  - content
  - excerpt
  - layout
---

This is the post content. It should be displayed in place of the user-defined excerpt in archive-index pages.

This paragraph should be absent from an archive-index page where `post.excerpt` is shown.
