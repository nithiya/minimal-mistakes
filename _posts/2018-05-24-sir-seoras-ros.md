---
title: Sir Seoras Ros
categories:
  - books
tags: 
  - books
  - scottish gaelic
header:
  overlay_image: /assets/images/scotland.jpg
  caption: "[*Berit Watkin. Scotland, mountains. Flickr. CC-BY-2.0.*](https://www.flickr.com/photos/ben124/5718687358/)"
  image_description: Scotland, mountains by Berit Watkin
  overlay_filter: .3
  actions:
    - label: "<i class='fas fa-fw fa-book' title='Source'></i> Source"
      url: https://www.gutenberg.org/ebooks/39849
---

San t-seann aimsir b'abhaist do na Gaidheil caoidh mór a dhèanamh mu
bhàs ceann-cinnidh, no neach eile air an robh mór spéis. B' e dleasanas
a' phiobaire an coronach a thogail ri toirm na pioba; dleasanas a'
bhàird cliù an laoich a chur air fonn nan dàn. B' i so an duais a b'
àirde ris an robh dùil aig na flaithean Gaidhealach 's an t-saoghal so,
maraon duais nam marbh agus misneachadh nam beo; oir cha robh àrd no
iosal gun deidh air deagh bheachd a ghinealaich fein, agus air àite
urramach fhaotainn ann an eachdraidh bhuan a dhuthcha.

Tha bàrdachd Oisein luchdaichte le trom thùrsa airson nam marbh gaisgeil
a chuir deagh chomhraig 's a thuit 's a bhlàr:--

> 'Charuill, tog do ghuth gu h-àrd \
> Air gach linn a bh' ann nach beò; \
> Caithear oidhche ann am mìn dhàn; \
> Faighear gàirdeachas 's a' bhròn. \
> 'S iomadh saoi is òigh 'bu chaoin \
> 'Ghluais o thùs a'n Innis-fàil. \
> Is taitneach dàin air na laoich \
> O thaobh Alba nam fuaim àrd. \
> 'N uair a dh' aomas farum na seilg \
> Fo ghuth Oisein nan caomh-rann, \
> 'S a fhreagras aonach an deirg \
> Sruth Chòna nan toirm mall. \
> --FIONN. D. I. 565

Agus, a rithis, mar tha e ag imeachd le ceum stàideil na h-aoise:--

> A ghutha Chòna, 's àirde fuaim, \
> A bhàrda tha luaidh mu aois, \
> Do'n eirich air 'ur n-anam suas, \
> Feachdan mór' nan gorm chruaidh laoch. \
> 'S taitneach leam aoibhneas a' bhròin, \
> Fo 'n lùb geug dharaig nan tòrr, \
> 'S an duileach òg ag eirigh maoth, \
> Togaibh-se, mo bhàird, am fonn.

Cha 'n fheil carrachan-cuimhne a's brèagha no a's maireannaiche na
iadsan a dheilbheadh leis na bàird mhóra,--Alasdair Mac-Mhaighstir
Alasdair, Dunncha Bàn Mac-an-t-saoir, Iain Lom, Iain MacCodrum, agus
Iain Moireastan, Gobha na Hearradh, mar air Domhnullach na Tòiseachd:--

> Bha do phearsa 's do ghluasad, \
> Co tlachdmhor, neò-thuairgneach, is tlàth, \
> Chor 's gur tearc iad thug fuath dhuit, \
> Ged bu lionmhor bha fuaidht' riut a'n gràdh; \
> Aig na h-ìslean 's na h-uaislean, \
> Bha spéis dhuit, ged b' uaibhreach dhiubh pàirt, \
> Ni dh' fhag t-iomradh g' a luaidh ac', \
> Mar sgeul bròin o'n là chual iad do bhàs.

Excerpt from *Gearr-sgeoil air Sir Seoras Uilleam Ross* by Alexander Fraser.
